'use strict'

const https = require('https');
const debug = require('debug')('node:server');
const express = require('express');

const app = express();
const port = 8080;
app.set('port',port);


const server = https.createServer(app);
const router = express.Router();


const route = router.get('/', (req, res, next) => {
    res.status(200).send({
        title: "node store api",
        version: "0.0.1"

    });
})

app.use('/', route);


server.listen(port);
console.log('api rodando na porta:' + port);